+++
title = 'Random Post About Things'
date = 2024-04-09T15:17:37-04:00
draft = false
tags = ['endode', 'vtuber', 'pngtuber', 'programming', 'game_dev']
+++
There haven't been posts recently, that's because there isn't really much for me to post about.\
I could talk about some random things I guess.

## Game Dev
Development on my game has been good, it's code has been better than Flutterby's, and I think I'm gonna to take some of this new code and put it into the new Flutterby once I start that project. Also about my current horror game project (Drawn), I have a camera in it, it's the main gimmick of the game. I'm concerned that people are gonna think I took the camera idea from Content Warning when I actually thought of it from OUTLAST and Fatal Frame (I don't know much about Fatal Frame other than the camera). I really shouldn't worry about it.

## Programming Languages
I've been thinking about programming languages recently, I think I want to explore Zig and maybe write some projects in C. If I end up liking Zig, then I might remake my game engine (Endgine) in Zig (or maybe C)

## Misc
I think I want to have my own VTuber, Auston (AustonBluBoy) has been doing it for awhile now and I like it. I'm thinking of making my own VTuber software called "Goober Toober". I'm unsure if I want to have a 3D model, or just an image (PNGTuber). I'd be concerned with having a camera looking at me constantly. Also another reason 3D would be cool, is that I have ordered SlimeVR trackers to use in VRChat, meaning I'd have fullbody tracking (which I probably wouldn't use as a VTuber but it's possible if I really wanted to). So actually I think I'm leaning PNGTuber right now. We'll see if I end up doing anything.
