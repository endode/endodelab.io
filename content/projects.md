+++
title = 'Projects'
date = 2024-02-05T15:56:36-05:00
tags = ['endode', 'projects']
menus = ['main', 'endode', 'projects']
+++
## Current Projects
- Flutterby ([Outdated Prototype is released (Which is kind of bad)]({{< ref "/posts/2024/flutterby/flutterby_prototype_available.md" >}}))
## Not Cancelled, Not Current Projects
They're projects I can always add something to or change, but aren't counted as Current (or Main) Projects
- Endgine (I'm actually **SLOWLY** rewriting my game engine in Zig, I'm not doing as much work on it as I want to)
- Femdode 3D Model
- Zinc (My Discord Bot, which is used on [Auston's server](https://discord.gg/DFRqc8q))
- [This Website](https://endode.gitlab.io/)
<!-- ## On Hold Projects -->
<!-- I'm working on other projects first, then work will (maybe) be continued on these projects -->
## Released Projects
Some other stuff could be added to this list, but they're not important enough
- [Vs. BluBoy (AustonBluBoy's FNF Mod)](https://gamebanana.com/mods/528459)
- [Endode's Silly Birthday Game]({{<ref "/posts/2024/endodes_silly_birthday_game.md">}})
- [Flutterby Beta Demo Prototype (Bad, I don't recommend you play this)]({{< ref "/posts/2024/flutterby/flutterby_prototype_available.md" >}})
- Quick Turn Mod / Vek Quick Turn for Minecraft\
[Modrinth](https://modrinth.com/mod/quick-turn-mod) - [CurseForge](https://www.curseforge.com/minecraft/mc-mods/quick-turn-mod)
- [Endode Steamboat Willie Horror Game]({{< ref "/posts/2024/steamboat_willie_horror_game.md" >}})
- [Endode Steamboat Willie Horror Game: Minnie's Manic]({{< ref "/posts/2024/steamboat_willie_horror_game_minnies_manic.md" >}})
- [Pwease Give Streamer Notif](https://github.com/Endode/pwease-give-streamer-notif)
## Basically Cancelled, but Could Go Back To Them... Projects
- A FNaF fangame (To be honest, my motivation for this has dropped)
