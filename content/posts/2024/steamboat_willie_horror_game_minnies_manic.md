+++
title = "Endode Steamboat Willie Horror Game: Minnie's Manic"
date = 2024-01-29T02:36:05-05:00
draft = false
tags = ['horror', 'game', 'game_dev', 'project', 'mickey_mouse', 'steamboat_willie', 'minnie_mouse']
+++
# Another One!
Here's the [itch.io](https://endode.itch.io/endode-steamboat-willie-horror-game-minnies-manic) and [GameJolt](https://gamejolt.com/games/endode-steamboat-willie-horror-game-minnies-manic/872979) links

That's right! I made a second one!

It's currently 2:30 AM when I'm writing this so I don't wanna type that much right now, I need to go to bed

This is still satire by the way

If people play this on YouTube then I'll put their videos here. If they play both games in one video then I might make a post specifically for videos of these games

PS: I may make a 3rd one at some point
