+++
title = 'Flutterby January Update Part 2'
date = 2024-01-26T19:11:18-05:00
draft = false
tags = ['flutterby', 'game', 'game_dev', 'project']
+++
## Guh?
Yep, this is a part 2 post
So, I said before I wanted to have Flutterby use my own game engine

I'm not sure if it's a good idea to do that anymore. Godot is just so much easier to work with than making my own game engine. I think it'll be smart to just stick with Godot and wait until I know what I'm doing with my engine.

This is something I keep going back and forth on, I keep changing my mind. So this might change again, but for right now, I'm going to use Godot. (Unless I change my mind without saying anything about it)

PS: At some point in the future, if my game engine improves enough, I might switch Flutterby to it.

PSS: I've even gone back and forth with my game engine. It's written in C++ and uses OpenGL (with Vulkan planned), but I'm also thinking of Rust and WGPU. So that's something else I'm debating.

EDIT: Even after writing this, I've changed my mind multiple times. Aaaaaaa
