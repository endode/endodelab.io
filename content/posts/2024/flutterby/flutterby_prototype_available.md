+++
title = 'Flutterby Prototype Available'
date = 2024-02-12T14:58:58-05:00
tags = ['flutterby', 'game', 'game_dev', 'project', 'prototype', 'downloadable']
+++
## Links
Here's the [itch.io](https://endode.itch.io/flutterby) and [GameJolt](https://gamejolt.com/games/flutterby/876177) links

## More Info
You may have remember that I said I was going to release this at the end of 2023, and yes, that's what I was going to do, it was ready to be released. But, Auston played it and thought I should work on it more before releasing it, then he said I should work on making little games, so that's what I'm doing. I've been working on little games this year (2024), but I haven't done much to Flutterby.

I've decided to release essentially what Auston played publically (on [his birthday stream](https://www.youtube.com/live/Xw4jWlhL890?si=64DWk0pQ0ng4JJ-m&t=2652)), because I want to rewrite and rework Flutterby. When I started working on Flutterby, I was still a bit new to Godot. That means a lot of the code or way I decided to do things in Flutterby is suboptimal, now I've learned and making these littles games have taught me much better ways to do things.

So that's what's happening, this prototype is not very great and is being remade

If you do play this, I hope you understand that this version is not my best work and it contains some problems, but is playable and beatable.
