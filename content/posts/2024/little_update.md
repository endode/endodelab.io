+++
title = 'Little Update'
date = 2024-05-10T19:40:27-04:00
draft = false
tags = ['endode', 'programming', 'fnaf', 'fnf']
+++
Hi!

## Five Nights at Freddy's
I'm gonna get right into it, so I think I'm gonna put the horror game I was working on on hold (Drawn). So instead of working on that, I'm actually working on a Five Nights at Freddy's Fangame. I've always wanted to make one, and I've tried before, but this time I'm gonna go finish it! I have some nice plans for it, and I'm using what I made for Drawn in this FNaF fangame.

## Friday Night Funkin
Something else I'm doing, is working on AustonBluBoy's FNF mod. I don't have much to say about it, everything has been coming along nicely. Also, the songs are bangers.

## Best Update
This post has been the best ever
