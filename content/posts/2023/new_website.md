+++
title = 'New Website'
date = 2023-11-25T15:47:28-05:00
draft = false
tags = ['website', 'hugo']
+++
That's right, I have a new website! I've been wanting to redo my website for a while now, and I've been thinking about using a static site generator\
That's what this is, this website uses Hugo
## Why
Limitations exist when writing straight HTML, CSS, and JS. It's not impossible, but some things are just a bit annoying. For example, you can have a header. You want this header to appear on every page, but that's a lot of duplication. If you want to update it, you'd need to update every single file.\
Hugo solves this issue, you can have a file for the header, and then include that file in every page.\
I say this like I've actually used Hugo, nope, I've just installed and slightly tweaked a theme, and just started writing this content; I haven't written any HTML here, but I know how it works.\
I actually want to try to write my own static site generator, it'd be simple and fun.
## For What Purpose?
I am going to use this website to make some occasional posts and to just have links to my stuff... BUT, I have just realized while writing this, is that I have thought about making some tutorials. I don't know for what yet, maybe Minecraft Modding, maybe Godot game tutorials, or maybe something else.\
So, maybe I'll have tutorials on here, but maybe not. I hope I at least try it out and make a couple.
## okay
okay\
That's really just it, I don't have much else to say.\
This website is just here to contain my info and stuff. Also for some tutorials hopefully.\
Oh and by my stuff, that includes programming/game dev projects.
