+++
title = 'Test Post'
date = 2023-11-25T13:24:51-05:00
draft = false
+++

This is just a test post, because I need to figure out how Hugo works

## Test Code Block
I gotta see how code blocks gets rendered here
```cpp
#include <iostream>

int main() {
    // In the original version of this code, I had used "\n" instead of std::endl;
    std::cout << "Hello World! :3" << std::endl;
    return 0;
}
```
