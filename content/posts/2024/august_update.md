+++
title = 'August Update'
date = 2024-08-14T18:57:01-04:00
draft = false
tags = ['endode', 'godot', 'femdode', 'blender']
+++
## New Flutterby and Godot
I started remaking Flutterby, I'm still using Godot but this time I'm using C#. It's been awesome so far. I actually had setup C++ GDExtension stuff and was gonna use that but some things were a little bit hard to find since not many people actually use GDExtension, so I decided it would just be better to try C#. The only thing I dislike about using C# in Godot is because it makes this data folder in the export that contains C# stuff like DLLs, it increased the exported games size and now it's not just two files, but oh well, it's not really a big deal and doesn't really matter much

## Femdode Plushie
If you don't know, I discovered that I'm nonbinary and genderfluid a little bit over a month from when I'm typing this. For me that means I'll go between feeling nonbinary, where I don't feel exactly male or female, to feeling fem (Well okay it can go towards masc a little bit but I really enjoy feeling fem). It's been awesome and it's made me realize this fem side that I have. So I made a human-looking fem version of Endode. So last night I decided to try to make a plushie model in Blender of it and it actually came out REALLY WELL, like I've impressed myself it's so awesome! I thought I was gonna make some decent looking plushie that had solid colors but nope I locked in at like 1-2 AM and I'm just so happy with it! This may be the start to my artist arc

![Femdode Plushie Spinning](/post_content/august_update/endode_fem_endode_plushie_spin.gif)
![Femdode Plushie Contained](/post_content/august_update/they_cannot_contain_me_for_long_time_is_running_out_fem_endode_plushie.png)
![Femdode Plushie Spinning Cube](/post_content/august_update/fem_endode_plushie_spinning_cube.gif)
![Femdode Plushie Still 1](/post_content/august_update/endode_fem_endode_plushie_still_render.png)
![Femdode Plushie Still 2](/post_content/august_update/fem_endode_plushie_square_face_still.png)
