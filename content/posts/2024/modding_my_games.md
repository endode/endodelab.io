+++
title = 'About Modding My Games'
date = 2024-01-31T17:55:47-05:00
draft = false
tags = ['modding', 'mods']
+++
You can do it :)

I love mods, anyone is allowed to make mods for my games

I'm thinking about making a small little mod loading system for my Godot games. Godot can load PCK files (files that contain game data and assets), and I think I might add the ability for the game to check for a "mods" folder and then have it automatically load any PCK files found in that folder. Meaning that modders can override the files that the game uses. This isn't implemented yet, but I think it would be a great idea
