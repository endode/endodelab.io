+++
title = 'Discord Test Post'
date = 2024-01-27T15:22:05-05:00
draft = false
tags = ['discord', 'test', 'rss']
+++
## Test Post!
This is just a test post!

I setup a Discord bot that'll look at the RSS every few minutes to check for posts. If it sees a new post, it should post about it in the post-notify channel on my Discord server

So uh, this post is meant to test that

Fun Fact: There is RSS support, the URL for it is just [https://endode.gitlab.io/index.xml](https://endode.gitlab.io/index.xml)
