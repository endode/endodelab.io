+++
title = '2024 and 100 YouTube Subscribers'
date = 2023-12-31T15:19:53-05:00
draft = false
tags = ['youtube', 'milestone', 'new_years', '2024']
+++

## 100 Subscribers
Today, as of December 31, 2023 (EST), I have hit 100 subscribers on YouTube. That's not very many compared to other channels, which have thousands or even millions of subscribers. But I think it's neat, especially since I don't take my content on YouTube very seriously. Also, I have been uploading YouTube shorts daily, and I'll continue doing that. Not forever though, I'll stop doing it eventually\
So that's a positive start to 2024\
![100 subscribers](/post_content/100_yt_subscribers/cat_real.png)
## 2024
So, 2024 is approaching. For me, it's still 2023, but soon, it'll be 2024\
Here's what my New Year's Resolutions (1920x1080) for 2024 are (Things will be crossed off as they are done (or not done and just canceled)):
- ~~Make the current version of Flutterby better and just improve upon on it a lot before releasing it as a public demo~~ (I didn't do this exactly)
- ~At least start work on Chapter 1 (the chapter after the Prologue) of Flutterby~~ (I'm now rewriting/remaking Flutterby still in Godot but using C# this time (UPDATE: This is going good))
- ~~Make some decent improvements to my game engine~~ (I'm actually rewriting my game engine. I'm actually using Zig now. I haven't really been making progress on this, I've been working on other things)
- ~~Make and release a small game~~
- Write and release a short story (I actually already wrote a short story, but it's too revealing of the lore)
- ~~Improve Blender skills~~

I wanted to keep my resolutions decently simple and realistic, as I have made them too unrealistic in the past.

NOTE (May 13th 2024): These weren't unrealistic, but things changed so a couple of them got invalidated/canceled/aren't relevant anymore.
