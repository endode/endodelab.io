+++
title = 'Endode'
date = 2024-01-14T13:14:40-05:00
tags = ['endode', 'about']
menus = ['main', 'endode', 'projects']
+++
I love computers, horror, and Linux. I love programming, specifically game development and game engine programming. I don't like web dev. I use neovim as my text editor. English is my native language, and I was learning German but I haven't practiced it much recently, instead I've been learning Japanese (and Korean)... which I haven't been practicing recently either. I'm a game developer. Godot is awesome. I have my own lore, but it hasn't really been expressed anywhere. I have a game I'm working on called Flutterby. On my puter!! Dink my offee!! :3\
I LOVE YOU KIMCHI/SOUP/ALEX/ALASTOR :33333 🫶 <3

Email: <a href="mailto:endode9@gmail.com">endode9@gmail.com</a>

Pronouns.page: <a href="https://en.pronouns.page/@endode">https://en.pronouns.page/@endode</a>

## Endode Colors
<a style="background:#920085">#920085</a>
<a style="background:#000469">#000469</a>
#### Lighter (for text):
<a style="color:#ff94f6;">#ff94f6</a>
<a style="color:#0086ff;">#0086ff</a>
<a style="color:#B55AFF;">#B55AFF</a>
#### Skin:
<a style="color:#fcc8b4;">#fcc8b4</a>
#### Hair:
<a style="color:#920085;">#920085</a>
<a style="color:#B259FB;">#B259FB</a>

## Not So Small Q&A
- Can I play one of your games on stream/in a video?
- *Yes, just please link to the game in the description or equivalent section*
- Did you know that for your Godot projects, you can have the PCK embedded inside the executable?
- *Yes, but I purposefully have the PCK separate to make it easier to mod*
- If you want to make it easier to mod, why do you have all the gd files encrypted and nothing else?
- *I'm shy, and that applies to my code. I don't want people to judge my code, so I'm hiding it (It's silly I know)*
- I found a bug
- *Cool, if it's game breaking then I'd want to know, if it's something difficult to do/you really need to go out of your way to do it, then I think it's okay, I'd still want to know about it either way. If it's a visual bug I think I still wanna know about it*
- Are you really silly?
- *Watermelon! 🍉*
- What languages do you speak/know?
- *English is my native language, I'm learning German (although I haven't been practicing it), and I've started learning Japanese (and Korean)*
- What programming/scripting languages do you know?
- *Language is not too important, learning how to program is more important (This is what tsoding/Rexim says, which I agree with)*
- What are you?
- *Really silly*
- Why is your YouTube channel a bunch of stuff? Why don't you organize it into separate channels?
- *My YouTube channel is the platform for my video content. My dream is NOT to be a YouTuber, so I do not care if the channel succeeds or not, my dream is to be a game developer*
- What do you do?
- *Silly activities! I am a game developer and programmer*
- How long have you known Auston (AustonBluBoy)
- *A long time*
- You're learning Japanese? How much Japanese do you know?
- *I just started, I've learned both Hiragana and Katakana but I haven't mastered them yet. I've started learning basic grammar*
- You're learning Korean now? How much Korean do you know?
- *Pretty much nothing, and I haven't practiced it for a few days, I really should though*
- Where do you live?
- *Pluto and Auston's Basement*
- Do you like web dev?
- *No*
- Are you gonna reorder your Q&A and put some of the more popular questions at the top?
- *Maybe*
- What text editor do you use?
- *neovim (In rare circumstances VS Codium (Haxe))*
- What is your favorite game engine?
- *Godot*
- What is your favorite programming language?
- *I don't know*
- Are you a nerd?
- *🤓*
- Favorite color?
- *Purple, blue*
- Are you an artist?
- *Sometimes*
- If you had a catchphrase, what would it be?
- *Either "okay" or "I don't know what I'm doing"*
- Do you know what the next Q&A question should be?
- *Yes, this one*
- Did people actually submit these questions?
- *No*
- Is this the end of the Q&A?
- *For now*

![Femdode Retro Low Poly Model Spinning](/femdode_retro_lp_2025_spin.gif)
![Femdode 3D Model Blender Screenshot](/femdode_cutie.png)
![Femdode Plushie Spinning](/post_content/august_update/endode_fem_endode_plushie_spin.gif)
![Endode's Former Profile Picture](/endode.png)
