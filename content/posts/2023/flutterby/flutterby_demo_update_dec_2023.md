+++
title = 'Flutterby Demo Update'
date = 2023-12-27T15:54:21-05:00
draft = false
tags = ['flutterby', 'game', 'game_dev', 'project', 'demo']
+++

## Update News
Originally, the Flutterby demo was going to be released by the end of this year (2023), but Auston (AustonBluBoy) played the demo on his birthday stream. He suggested that I update and improve the demo before releasing it. So that's what I am going to do.
## So when will it be released?
The demo will 100% be released next year (2024), I hope for a Q1 of the year release. I am going to be implementing changes Auston suggested, and adding more content to make the demo longer than it is, because it is really really short and boring right now. I am going to be reworking some of the mechanics and code that currently exists for the game as some of it is a bit messy.
## Can I see what the demo looked like?
Well, AustonBluBoy played it on his birthday stream. Here's a link to that, the demo starts at 44:26: https://www.youtube.com/live/Xw4jWlhL890?si=JrO4qkxi8RbsFlS4&t=2666
## itch.io Page
I quickly wanna put a link here to the itch.io page, which probably won't be the only place the game is hosted on, but here's a link to that page: https://endode.itch.io/flutterby
