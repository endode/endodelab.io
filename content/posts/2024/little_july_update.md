+++
title = 'Little July Update'
date = 2024-07-03T12:58:41-04:00
draft = false
tags = ['endode', 'amd', 'nvidia', 'gpu', 'godot', 'fnaf']
+++
## New GPU
I decided to get a new GPU. I used to have a NVIDIA RTX 2060, but now I have an AMD RX 7700 XT! Part of the reason I decided to do this upgrade was because 6GB of VRAM is limiting in VR, and the 12GB that the RX 7700 XT has been great! Another reason is because I'm primarily a Linux user, and AMD is just awesome on Linux! I've tested a lot of games and haven't had many issues, it's been great so far. All my games work (so does Vs. BluBoy, although I did have to edit a shader but it was a mistake I made that NVIDIA didn't mind, but Mesa did)

## Godot GDExtension
All my Godot games use 100% GDScript, which I'm wondering if that has been a mistake or not. My Godot games do perform well, they don't have performance issues, but I'm wondering if I should switch some code over to GDExtension. I don't know if it's worth it, I'll complicate how I build and release my games, and I don't have any issues with GDScript right now, so we'll see what I do.

## FNaF Fangame
I've been working on my FNaF fangame and it's been going along well so far. Not really much of gameplay yet, but it's coming along. I've been making some basic assets and core functionality, I have a lot of the game planned out already and I like how it's going. No promises, but I hope to finish the game before the end of the year.
