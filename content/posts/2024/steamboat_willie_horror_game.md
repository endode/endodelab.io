+++
title = 'Endode Steamboat Willie Horror Game'
date = 2024-01-06T12:18:17-05:00
draft = false
tags = ['horror', 'game', 'game_dev', 'project', 'mickey_mouse', 'steamboat_willie']
+++
## Endode Steamboat Willie Horror Game
tldr here's the [itch.io page](https://endode.itch.io/endode-steamboat-willie-horror-game) and [Game Jolt page](https://gamejolt.com/games/endode-steamboat-willie-horror-game/868726)  
Yep, that's the title: "Endode Steamboat Willie Horror Game"  
Because Steamboat Willie became part of the public domain, I decided to make a statire purposefully bad horror game about it. I basically spent 3 days on it, it's pretty bad, but that's good because that was my intention. It's not meant to be taken seriously, and I purposefully made mistakes and didn't put *too* much effort in, although there was some effort put in, mostly into programming.

This was actually decently educational, as it's the first time I've actually done 3D in Godot. I have previously mostly just done 2D.  
You can play it right now on itch.io and Game Jolt, it's the first time I've ever released anything on itch.io. Here's the [itch.io page](https://endode.itch.io/endode-steamboat-willie-horror-game) and [Game Jolt page](https://gamejolt.com/games/endode-steamboat-willie-horror-game/868726)  
So overall, I'm pretty happy with it. I reached my goal and released the game. The game is how I want it to be, bad, unpolished, satire, it's purposefully low quality.  

Okay I'll stop yapping now

Hey wait this is the first post of 2024. Oh, and this actually already crosses off one of my New Years Resolutions from my list "Make and release a small game"  
Yippee!
## Someone played it
Someone actually played it and uploaded it to YouTube, so here's their video
{{< youtube d7wOditoOHU >}}
They actually played the game again since I told them about the bug
{{< youtube 3la2EZFFD20 >}}
## Another person has played it
In this video they played 2 different games, my game is the second one
{{< youtube qnR7p2J3ucM >}}
## Yet another person
They played 3 different games, my game is the third one. I don't think they realized my game is a joke and intentionally bad
{{< youtube Y58GFmpd2h8 >}}
## And another person has played it
The gameplay for my game starts at 2:37:29
{{< youtube tkCLk0EdUNg >}}
