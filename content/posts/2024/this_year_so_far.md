+++
title = 'This Year So Far'
date = 2024-01-29T18:25:20-05:00
draft = false
tags = ['2024']
+++
## It's Going Good
My 2024 has been pretty good so far. I've released two small games and that's made me happy. It was nice to see a couple people make videos on the Steamboat Willie Horror Game. This is definitely a good motivational boost, it makes me really want to finish projects so I can release them.

I've also worked on other projects, like my game engine. I've been making progress on it.

## Endode's Year?
I don't normally say stuff like "This will be my year!", but I feel like it is my year. At the end of last year, I had been uploading shorts that were doing decent, around 2K views per short (some did worse, some did better). I've just been productive, I feel like I've learned some things. The Steamboat Willie Horror Games that taught be more about Godot, mainly 3D. That knowledge is definitely going to come in handy.

I just feel so good about this year :)
