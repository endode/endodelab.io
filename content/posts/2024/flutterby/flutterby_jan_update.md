+++
title = 'Flutterby January Update'
date = 2024-01-18T22:29:06-05:00
draft = false
tags = ['flutterby', 'game', 'game_dev', 'project']
+++
## tldr
My dream is to make a game engine, so I'm doing that, and Flutterby is a mess, so it makes sense to remake Flutterby in my engine. Also might release Flutterby Godot version despite it's flaws
## Godot or Endgine
So, I have been doing a lot of debating in my head whether Flutterby should continue using Godot, or if it should use my own engine instead.
## Godot makes sense, but I'm Endode
For most people, it would probably make more sense to use Godot, as it's an already existing engine and game engines are hard to develop. But I'm not most people, I'm Endode.
## Endgine
A dream I have is to make my own game engine. My engine doesn't have to be fancy and or feature rich, it just needs to be decent and work. In fact, I've already been developing my own engine, it actually got renamed a few days ago. It used to be called "Vekgine" but now it's "Endgine".
## Flutterby
Flutterby in it's current state is a bit messy and annoying to work with. I messed up when making Flutterby, the code isn't very pretty or the best. I want to remake parts of the game, parts of the Flutterby itself. It would be nice to start from scratch.
## Rewritten Flutterby in Endgine
Since I'm already making my own engine and because Flutterby needs some refactoring/rewriting, I think it's pretty logical to just make Flutterby in my engine. So that's what I am going to do. Flutterby is currently just getting keyboard input, moving sprites on the screen, playing audio, and loading JSON. It's not that complicated. In fact, my engine can already do all of that minus the JSON loading (Well okay the 2D support technically does function but it needs more work). Also, my engine supports 3D too, well okay it is just unlit textured OBJ models, but still.
## TODO for Endgine
Endgine is not yet ready for Flutterby. I still need to implement a few things, like scene saving and loading, UI, text that can update (text is just static right now), controller support, a scripting system (I want to use C# Mono, but I might end up using LuaJIT), and more. So a lot of the work that I've been doing recently has been on Endgine, I'm trying to get it to a state where I can start rewriting Flutterby. Also, I've decided that I'm going to have my engine, Endgine, work similarly to Godot, where the game and the engine are separate. I want there to be an executable and then some sort of compressed and maybe encrypted asset package that defines the game. I'm yapping a bit here, going to stop that now.
## Possible Flutterby Godot Version Release
That's right, since I want to remake Flutterby, I'm thinking of releasing the Godot version. This might not happen though, I want people's first impressions to be positive, and with the issues the Godot version has I don't know if people will like it. So maybe I'll release it, or maybe I won't. I mean, I'm sure at some point it'll be released, whether that's in a year (or two?) when the Endgine version is released or during some other point. Don't get your hopes up

Okay I'm gonna stop yapping now
