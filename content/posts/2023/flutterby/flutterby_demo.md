+++
title = 'Flutterby Demo'
date = 2023-11-25T16:09:33-05:00
draft = false
tags = ['flutterby', 'game', 'game_dev', 'project', 'demo']
+++

## Flutterby is my game that I've been developing for over a year now
The game is currently made in Godot 4, although I may port it over to my own game engine (Vekgine) when that is more complete and usable.\
It's a story-based RPG game currently without any sort of battle system, as I'm unsure whether or not I should have a battle system.\
At the time of writing this, the game is in Beta and includes some unfinished assets and is close to but not yet completable.

## Demo
I plan on releasing a demo of the game before the year of this year (2023).\
The demo includes the Prologue chapter of the game, which will be completable.\
Despite me thinking this chapter is close to completion, I have realized something that the game is missing that would make it a lot more fun. I will have to start developing this after the demo gets released.\
I promised to release something by the end of the year, I promised to release a demo of my game, and I'm sticking by that promise. It may not be as good as I want it to be, but I know that I will be given some feedback by my friend's followers/fans, oh and my by friend himself.\

## How I feel 'bout the game so far
I feel like the game in it's current state is honestly boring. There is some story and lore sprinkled around, but honestly there is no music (except for a placeholder that's being used in the main menu), and the player just gets to read some text, walk over somewhere, read more text, go somewhere else, interact (press a key on their keyboard/button on their controller), read some text, go somewhere else, interact and read some text, etc. It's not fun, it's a bit boring. But hey, it's the beginning of the game, it's the prologue, it's alright if it's not the most interesting and exciting part of the game. Yes that makes sense, but I have realized what's missing, and I plan on adding that in the future, but the demo won't have that.\
I really like the story of my game, almost all of it won't really be expressed at all in the prologue. I think it's a good story, but hey, I'm probably biased.\
When talking about something I think that the game is lacking, the visuals come to my mind. It's all programmer art, by me. I think the art looks decent, but it's nothinggreat, it's alright, it does its job.

Despite all of this, I still love my game, everything has problems, nothing is perfect. I have made a lot of progress in making the game, I had to make all the systems myself (excluding stuff that Godot already has). It's not like I used RPG Maker, which is already setup for RPG games. I had to learn things, and write a lot of code myself. During the development, I've also had to create concept art, story, pixel art, etc. I have a plan laid out for the game, I have an idea of what I want to be the main theme of each chapter, I already have some of the ending planned out, I even have a twist planned for the game that I'll probably start working on after the demo gets released.\
You know, I don't think I'm very good at writing. Not about writing code, but about writing text like this. I don't think I do a very good job at it, but that's okay.\
Anyways, I do wanna say that there will be something before the demo. Auston (AustonBluBoy) will be getting a streamer build of the game/demo, it'll be a less complete version of the demo that isn't as polished and has some more placeholders in it than the final demo will./
So keep on the lookout for that, as I plan on giving him that build very soon (~~within a week hopefully~~ He said he'd play it on his birthday stream, which will be on December 24th :3)./

One last thing, if you see this, please tell me. I wanna know if anyone actually knows this new site exists. (Yes I know this site has no social links or anything like that, you can visit [https://vekwrite.gitlab.io/endode/endode.html](https://vekwrite.gitlab.io/endode/endode.html) for those)
