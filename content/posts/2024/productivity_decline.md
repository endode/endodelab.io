+++
title = 'A Decline in Productivity'
date = 2024-03-03T14:32:18-05:00
draft = false
tags = ['productivity', '2024', 'projects']
+++
## My Productivity Declined
You know how at the beginning of this year (2024) I said I was super productive? [(here)]({{< ref "/posts/2024/this_year_so_far.md" >}}) Well uh I had a crash in that productivity. For the past couple of weeks I've barely done any real work on my projects. So not that I haven't done ANYTHING, as I have implemented some smaller things into my game and done a small amount of concept stuff, and I've written lots of notes and lore. But not much real work has been done, but I'm trying to change that.
## I'm going to change that
I'm going to go and do some real work today. In fact, I was about to do some real work right now but I remembered I was going to make this post, so I'm writing this post.

PS: Bocchi the Rock is awesome!

EDIT: I did end up doing real work, so that's good :)

EDIT 2 (March 4th 2024): Hey, it's the day after I wrote this post, and I was also productive today :)
