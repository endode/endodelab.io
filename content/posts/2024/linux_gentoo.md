+++
title = 'I Switched to Gentoo Linux'
date = 2024-03-14T12:59:14-04:00
draft = false
tags = ['linux', 'gentoo', 'computer', 'nerd', 'arch', 'artix', 'distrohopping', 'opensuse']
+++
## Linux?
I'm assuming you know what Linux is, because I don't want to explain what it is here

## Artix Linux
I used to use Artix Linux, it is basically Arch Linux but it lets you use different init systems. I used the OpenRC version. I used it for a little over 2 years, and I liked it, it's good. The problem with it is the stability. Arch/Artix is bleeding-edge, meaning you'll be using the latest packages, meaning you'll be on the edge of the packages, meaning you'll also be bleeding. This means that the latest packages haven't been tested for that much about of time, and they'll have issues. You'll update and then suddenly some program or part of your system just doesn't work right anymore. That's so annoying, it's made me realize how important stability is.
## Gentoo Linux
Gentoo is a source-based distribution, meaning you download the source code of a package and compile it yourself. This means installing packages takes long, but it also means you can configure the packages, you can include or exclude parts of the programs you do or do not need. You also compile the packages for your specific CPU. All of that gives you some more performance. A main aspect of Gentoo to me is the stability, I read in a few different places that Gentoo is more stable that Arch/Artix, and that seems to be true. (Although I can't speak much on that right now as I only installed it 3 days ago). I installed and messed with Gentoo in a VM for a couple of weeks, decided I liked it, and I installed it on my actual hardware. I'm using it to make this post.

## openSUSE?
I had a sour moment with Gentoo yesterday that I'm not gonna talk about, but it made me look into openSUSE a little bit. I don't really distrohop, but I think I might look at openSUSE. It seems to be underrated and very stable while still having pretty update-to-date packages.

But I'm not sure if I'll switch to openSUSE soon, Gentoo is working good so far

EDIT: Not switching to openSUSE, liking Gentoo

## tldr
I switched from Artix to Gentoo because of stability ~~and because I'm a little bit of a performance slut (a little bit, not entirely).~~ Also openSUSE looks good, but I'm sticking with Gentoo
