+++
title = '2025 is Approaching'
date = 2024-12-30T15:59:38-05:00
tags = ['new_years', '2024', '2025']
+++

## 2024
I think 2024 really was my year, it was great! I feel like I've improved as a programmer and a game developer. I created Femdode this year, the plushie, and main model, everything. Femdode is great, she's my trans self. I've improved my Blender skills, since I made the Femdode models. Zinc (my Discord bot) was rewritten in C++. Auston's FNF mod came out this year ([Vs. BluBoy](https://gamebanana.com/mods/528459)), which I worked on\
I think the most important thing this year, is that I got my first ever partner, his name is Kimchi/Soup/Alex/Alastor and he's very cute and lovely and sweet and I love him **very** much <3 (I love him so much, and I think about him all the time, and it causes my productivity to decline (silly))\
There are probably some other things I'm not thinking about, so I'll have to update this post once I think of them

## 2025
It's coming... SOOOOOOOO here's my New Year's resolutions (not in any specific order):
- Actually write a short story and release it (I've had this as a New Years resolution for years, I need to do it)
- Learn Korean (for my BF :3)
- EITHER make an improved Femdode 3D model OR making more outfits for the current Femdode model/improving it in general
- Finish the first chapter (Prologue) of Flutterby and release it
- Have my game engine be functional
- Make a Friday Night Funkin' (FNF) engine with my game engine
- Port [Vs. BluBoy](https://gamebanana.com/mods/528459) to my FNF engine
- ~~Fully switch from using X11 to Wayland (I've had Hyprland installed, and I use it sometimes, but I still primarily use awesomewm)~~ 🤓 (Technically for my drawing tablet, I'll still need to use awesomewm, last time I tried it in Hyprland it didn't work, so I installed open source drivers and it kind of worked but the calibration was off)
- *Maybe* make some music (I'll definitely do some music production, but idk if I'll focus on it a lot)

I will be updating this page during 2025, crossing things off, adding notes, etc
