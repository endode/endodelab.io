+++
title = "Endode's Silly Birthday Game"
date = 2024-06-08T20:17:10-04:00
draft = false
tags = ['horror', 'game', 'game_dev', 'project', 'endodes_silly_birthday_game', 'downloadable']
+++

{{< youtube 8qymxV4kcoA>}}

itch.io: https://endode.itch.io/endodes-silly-birthday-game

GameJolt: https://gamejolt.com/games/endodes-silly-birthday-game/902629

You're at your house, in your room, at your computer. Everything is normal, nothing is wrong, and then this creature just appears, what do you do?

Oh, it's also the creature's birthday, June 9th!

This game is half serious and half joke, mostly joke

This game is controller and Steam Deck compatible!
